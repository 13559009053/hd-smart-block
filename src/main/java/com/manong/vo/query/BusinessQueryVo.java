package com.manong.vo.query;

import com.manong.entity.business.Business;
import lombok.Data;

@Data
public class BusinessQueryVo extends Business {
    private Long pageNo = 1L;//当前页码
    private Long pageSize = 10L;//每页显示数量
}
