package com.manong.vo.query.vehicle;

import com.manong.entity.vehicle.VehicleLocationInformation;
import lombok.Data;

@Data
public class VehicleLocationInformationQueryVo extends VehicleLocationInformation {
    private Long pageNo = 1L;//当前页码
    private Long pageSize = 10L;//每页显示数量
}
