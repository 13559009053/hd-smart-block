package com.manong.vo.query.vehicle;

import com.manong.entity.vehicle.VehicleUser;
import lombok.Data;

@Data
public class VehicleUserQueryVo extends VehicleUser {
    private Long pageNo = 1L;//当前页码
    private Long pageSize = 10L;//每页显示数量
}
