package com.manong.vo.query.keypersonnel;

import com.manong.entity.keyPersonnel.KeyPersonnelBasicInformation;
import lombok.Data;

@Data
public class KeyPersonnelQueryVo extends KeyPersonnelBasicInformation {
    private Long pageNo = 1L;//当前页码
    private Long pageSize = 10L;//每页显示数量
}
