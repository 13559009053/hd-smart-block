package com.manong.vo.query;

import com.manong.entity.DeviceInformation;
import lombok.Data;

@Data
public class DeviceInformationQueryVo extends DeviceInformation {
    private Long pageNo = 1L;//当前页码
    private Long pageSize = 10L;//每页显示数量
}
