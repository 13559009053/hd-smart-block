package com.manong.vo.query;

import com.manong.entity.SensorInformation;
import lombok.Data;

@Data
public class SensorInformationQueryVo extends SensorInformation {
    private Long pageNo = 1L;//当前页码
    private Long pageSize = 10L;//每页显示数量
}
