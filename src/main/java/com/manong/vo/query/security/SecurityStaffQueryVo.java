package com.manong.vo.query.security;

import com.manong.entity.security.SecurityStaff;
import lombok.Data;

@Data
public class SecurityStaffQueryVo extends SecurityStaff {
    private Long pageNo = 1L;//当前页码
    private Long pageSize = 10L;//每页显示数量
}
