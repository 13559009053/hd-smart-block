package com.manong.vo;

import com.manong.entity.Department;
import lombok.Data;

@Data
public class DepartmentQueryVo extends Department {
}
