package com.manong.dao.test;

import com.manong.entity.test.Test;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface TestMapper extends BaseMapper<Test> {

}
