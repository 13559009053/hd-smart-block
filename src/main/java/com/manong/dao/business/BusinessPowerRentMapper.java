package com.manong.dao.business;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.manong.entity.business.BusinessPowerRent;

public interface BusinessPowerRentMapper extends BaseMapper<BusinessPowerRent> {

}
