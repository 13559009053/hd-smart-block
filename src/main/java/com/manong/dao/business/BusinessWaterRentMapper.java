package com.manong.dao.business;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.manong.entity.business.BusinessWaterRent;

public interface BusinessWaterRentMapper extends BaseMapper<BusinessWaterRent> {

}
