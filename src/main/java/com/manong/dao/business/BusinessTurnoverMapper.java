package com.manong.dao.business;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.manong.entity.business.BusinessTurnover;

public interface BusinessTurnoverMapper extends BaseMapper<BusinessTurnover> {

}
