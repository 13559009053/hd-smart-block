package com.manong.dao.business;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.manong.entity.business.Business;
import org.apache.ibatis.annotations.Select;

public interface BusinessMapper extends BaseMapper<Business> {
}
