package com.manong.dao.keyPersonnel;

import com.manong.entity.keyPersonnel.KeyPersonnelBasicInformation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface KeyPersonnelBasicInformationMapper extends BaseMapper<KeyPersonnelBasicInformation> {

}
