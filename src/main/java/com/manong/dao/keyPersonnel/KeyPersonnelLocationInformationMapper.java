package com.manong.dao.keyPersonnel;

import com.manong.entity.keyPersonnel.KeyPersonnelLocationInformation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

public interface KeyPersonnelLocationInformationMapper extends BaseMapper<KeyPersonnelLocationInformation> {
    /**
     *通过personnelId查询重点人员位置信息
     */
    @Select("select * from `sys_key_personnel_location_information` where personnel_id = #{personnelId}")
    KeyPersonnelLocationInformation findKeyPersonnelLocationInformationByPersonnelId(Integer personnelId);
}
