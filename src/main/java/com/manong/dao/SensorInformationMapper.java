package com.manong.dao;

import com.manong.entity.SensorInformation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.ArrayList;

/**
 * <p>
 * `sensor_information` Mapper 接口
 * </p>
 *
 * @author czm
 * @since 2023-02-04
 */
public interface SensorInformationMapper extends BaseMapper<SensorInformation> {
    /**
     *通过name查询硬件信息列表
     */
    @Select("select * from `sys_sensor_information` where name = #{name}")
    ArrayList<SensorInformation> findSensorInformationByName(String name);

    /**
     *通过userName查询5条用户操作硬件基本信息
     */
    @Select("select * from (select * from `sys_sensor_information` where user_name=#{userName} order by sys_sensor_information.time desc limit 5) as a ORDER BY a.id")
    ArrayList<SensorInformation> findSensorInformationByUserName(String userName);

    /**
     *查询未绑定硬件
     */
    @Select("select * from sys_sensor_information where id not in(select sensor_id from sys_sensor_data)")
    ArrayList<SensorInformation> findSensorInformationNoBind();
    /**
     *查询5条用户绑定硬件
     */
    @Select("select * from (select * from sys_sensor_information where id in(select sensor_id from sys_sensor_data) and user_name=#{userName} ORDER BY sys_sensor_information.id DESC LIMIT 5) as a ORDER BY a.id")
    ArrayList<SensorInformation> findSensorInformationBindByUserName(String userName);
    /**
     *通过name查询硬件个数
     */
    @Select("select count(id) from sys_sensor_information where name=#{name}")
    int postSensorInformationCountByName(String name);

    /**
     *通过设备Id查询传感器列表
     */
    @Select("select * from `sys_sensor_information` where device_id = #{deviceId}")
    ArrayList<SensorInformation> findSensorInformationByDeviceId(String deviceId);
    /**
     *通过id查询硬件信息列表
     */
    @Select("select * from `sys_sensor_information` where id = #{id}")
    ArrayList<SensorInformation> findSensorInformationById(Integer id);
    /**
     *查询7条用户绑定硬件
     */
    @Select("select * from (select * from sys_sensor_information where id in(select sensor_id from sys_sensor_data) and user_name=#{userName} ORDER BY sys_sensor_information.id DESC LIMIT 7) as a ORDER BY a.id")
    ArrayList<SensorInformation> findSensorInformationBindByUserNameSeven(String userName);
}
