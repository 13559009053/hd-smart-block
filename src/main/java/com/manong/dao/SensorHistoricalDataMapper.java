package com.manong.dao;

import com.manong.entity.SensorHistoricalData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.manong.entity.SensorInformation;
import org.apache.ibatis.annotations.Select;

import java.util.ArrayList;

public interface SensorHistoricalDataMapper extends BaseMapper<SensorHistoricalData> {

    /**
     *通过deviceId查询最新设备7条记录
     */
    @Select("select * from (select * from `sys_sensor_historical_data` where device_id=#{deviceId} order by sys_sensor_historical_data.time desc limit 7) as a ORDER BY a.id")
    ArrayList<SensorHistoricalData> findSensorHistoricalDataByDeviceId(String deviceId);
    /**
     *通过deviceId查询最新设备16条记录
     */
    @Select("select * from (select * from `sys_sensor_historical_data` where device_id=#{deviceId} order by sys_sensor_historical_data.time desc limit 16) as a ORDER BY a.id")
    ArrayList<SensorHistoricalData> findSensorHistoricalDataByDeviceIdSixteen(String deviceId);

    /**
     *筛选出温度大于25的记录返回个数
     */
    @Select("select count(id) from sys_sensor_historical_data where temperature>25")
    int findUnusualNumber();
    /**
     *筛选出湿度大于100的记录返回个数
     */
    @Select("select count(id) from sys_sensor_historical_data where humidity>100")
    int findUnusualNumberHumidity();
    /**
     *筛选出光敏大于200的记录返回个数
     */
    @Select("select count(id) from sys_sensor_historical_data where illumination>100")
    int findUnusualNumberIllumination();
}
