package com.manong.dao.vehicle;

import com.manong.entity.vehicle.VehicleBasicInformation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

public interface VehicleBasicInformationMapper extends BaseMapper<VehicleBasicInformation> {

    /**
     *通过carNumber查询车辆列表
     */
    @Select("select * from `sys_vehicle_basic_information` where car_number = #{carNumber}")
    VehicleBasicInformation findVehicleBasicInformationByCarNumber(String carNumber);
}
