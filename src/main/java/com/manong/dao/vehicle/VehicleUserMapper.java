package com.manong.dao.vehicle;

import com.manong.entity.vehicle.VehicleUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface VehicleUserMapper extends BaseMapper<VehicleUser> {

}
