package com.manong.dao.vehicle;

import com.manong.entity.vehicle.VehicleLocationInformation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface VehicleLocationInformationMapper extends BaseMapper<VehicleLocationInformation> {

}
