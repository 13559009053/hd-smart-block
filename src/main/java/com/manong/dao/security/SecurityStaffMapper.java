package com.manong.dao.security;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.manong.entity.security.SecurityStaff;

public interface SecurityStaffMapper extends BaseMapper<SecurityStaff> {

}
