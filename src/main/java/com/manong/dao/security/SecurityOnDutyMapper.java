package com.manong.dao.security;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.manong.entity.security.SecurityOnDuty;
import org.apache.ibatis.annotations.Select;

public interface SecurityOnDutyMapper extends BaseMapper<SecurityOnDuty> {

    /**
     *通过dutyId查询值班信息
     */
    @Select("select * from `sys_security_on_duty` where id = #{dutyId}")
    SecurityOnDuty findSecurityOnDutyById(Integer dutyId);
}
