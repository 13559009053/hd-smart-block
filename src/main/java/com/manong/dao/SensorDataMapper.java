package com.manong.dao;

import com.manong.entity.SensorData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author czm
 * @since 2023-02-04
 */
public interface SensorDataMapper extends BaseMapper<SensorData> {
    /**
     *通过sensorId查询实时数据列表
     */
    @Select("select * from `sys_sensor_data` where sensor_id = #{sensorId}")
    SensorData findSensorDataBySensorId(Integer sensorId);

    /**
     *通过sensorId更新实时数据列表
     */
    @Update("update `sys_sensor_data` set id=#{id},temperature=#{temperature},humidity=#{humidity},illumination=#{illumination},photo=#{photo},video=#{video} where sensor_id = #{sensorId}")
    int updateBySensorId(SensorData sensorData);
}
