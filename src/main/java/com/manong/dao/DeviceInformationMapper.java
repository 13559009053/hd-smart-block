package com.manong.dao;

import com.manong.entity.DeviceInformation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.ArrayList;

public interface DeviceInformationMapper extends BaseMapper<DeviceInformation> {
    /**
     *通过设备ID查询设备列表
     */
    @Select("select * from `sys_device_information` where id = #{deviceId}")
    DeviceInformation findDeviceInformationByDeviceId(String deviceId);
    /**
     *通过deviceName查询设备列表
     */
    @Select("select * from `sys_device_information` where name = #{deviceName}")
    ArrayList<DeviceInformation> findDeviceInformationByDeviceName(String deviceName);
    /**
     *添加设备
     */
    @Insert("INSERT INTO sys_device_information(id, name, lng, lat, creat_time) VALUES  ( #{id}, #{name}, #{lng}, #{lat}, #{creatTime} )")
    boolean add(DeviceInformation deviceInformation);

    @Update("update `sys_device_information` set name=#{name},lng=#{lng},lat=#{lat},creat_time=#{creatTime} where id = #{id}")
    boolean updateDeviceInformationById(DeviceInformation deviceInformation);

    @Delete("delete from sys_device_information where id=#{id}")
    boolean deleteDeviceInformationById(String id);
}
