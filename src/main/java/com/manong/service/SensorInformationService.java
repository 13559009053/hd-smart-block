package com.manong.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.manong.entity.Role;
import com.manong.entity.SensorInformation;
import com.baomidou.mybatisplus.extension.service.IService;
import com.manong.vo.query.SensorInformationQueryVo;

import java.util.ArrayList;

public interface SensorInformationService extends IService<SensorInformation> {

    ArrayList<SensorInformation> findSensorInformationByName(String name);

    ArrayList<SensorInformation> findSensorInformationByUserName(String userName);

    ArrayList<SensorInformation> findSensorInformationNoBind();

    ArrayList<SensorInformation> findSensorInformationBindByUserName(String userName);

    int postSensorInformationCountByName(String name);

    IPage<SensorInformation> findSensorListByName(IPage<SensorInformation> page, SensorInformationQueryVo sensorInformationQueryVo);

    ArrayList<SensorInformation> findSensorInformationByDeviceId(String deviceId);

    ArrayList<SensorInformation> findSensorInformationById(Integer id);

    ArrayList<SensorInformation> findSensorInformationBindByUserNameSeven(String userName);
}
