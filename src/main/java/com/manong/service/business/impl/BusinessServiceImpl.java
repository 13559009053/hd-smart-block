package com.manong.service.business.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.manong.dao.business.BusinessMapper;
import com.manong.entity.SensorData;
import com.manong.entity.business.Business;
import com.manong.service.business.BusinessService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manong.vo.query.BusinessQueryVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

@Service
@Transactional
public class BusinessServiceImpl extends ServiceImpl<BusinessMapper, Business> implements BusinessService {

    @Override
    public IPage<Business> findBusinessListByName(IPage<Business> page, BusinessQueryVo businessQueryVo) {
        //创建条件构造器
        QueryWrapper<Business> queryWrapper = new QueryWrapper<Business>();
        //商店姓名
        queryWrapper.like(!ObjectUtils.isEmpty(businessQueryVo.getName()),"name",businessQueryVo.getName());
        //排序
        queryWrapper.orderByAsc("id");
        return baseMapper.selectPage(page,queryWrapper);
    }

    @Override
    public IPage<Business> findBusinessListByType(IPage<Business> page, BusinessQueryVo businessQueryVo) {
        //创建条件构造器
        QueryWrapper<Business> queryWrapper = new QueryWrapper<Business>();
        queryWrapper.eq("type",businessQueryVo.getType());
        //排序
        queryWrapper.orderByAsc("id");
        return baseMapper.selectPage(page,queryWrapper);
    }
}
