package com.manong.service.business.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.manong.dao.business.BusinessPowerRentMapper;
import com.manong.entity.business.BusinessPowerRent;
import com.manong.service.business.BusinessPowerRentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manong.vo.query.BusinessPowerRentQueryVo;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
public class BusinessPowerRentServiceImpl extends ServiceImpl<BusinessPowerRentMapper, BusinessPowerRent> implements BusinessPowerRentService {

    @Override
    public IPage<BusinessPowerRent> findBusinessPowerRentListByMoney(IPage<BusinessPowerRent> page, BusinessPowerRentQueryVo businessPowerRentQueryVo) {
        //创建条件构造器
        QueryWrapper<BusinessPowerRent> queryWrapper = new QueryWrapper<BusinessPowerRent>();
        //电费
        queryWrapper.like(!ObjectUtils.isEmpty(businessPowerRentQueryVo.getMoney()),"money",businessPowerRentQueryVo.getMoney());
        //排序
        queryWrapper.orderByAsc("id");
        return baseMapper.selectPage(page,queryWrapper);
    }
}
