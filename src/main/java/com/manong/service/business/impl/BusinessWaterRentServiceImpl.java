package com.manong.service.business.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.manong.entity.business.BusinessTurnover;
import com.manong.service.business.BusinessWaterRentService;
import com.manong.dao.business.BusinessWaterRentMapper;
import com.manong.entity.business.BusinessWaterRent;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manong.vo.query.BusinessWaterRentQueryVo;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;


@Service
public class BusinessWaterRentServiceImpl extends ServiceImpl<BusinessWaterRentMapper, BusinessWaterRent> implements BusinessWaterRentService {

    @Override
    public IPage<BusinessWaterRent> findBusinessWaterRentListByMoney(IPage<BusinessWaterRent> page, BusinessWaterRentQueryVo businessWaterRentQueryVo) {
        //创建条件构造器
        QueryWrapper<BusinessWaterRent> queryWrapper = new QueryWrapper<BusinessWaterRent>();
        //水费
        queryWrapper.like(!ObjectUtils.isEmpty(businessWaterRentQueryVo.getMoney()),"money",businessWaterRentQueryVo.getMoney());
        //排序
        queryWrapper.orderByAsc("id");
        return baseMapper.selectPage(page,queryWrapper);
    }
}
