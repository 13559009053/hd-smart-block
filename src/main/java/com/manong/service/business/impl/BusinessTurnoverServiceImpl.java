package com.manong.service.business.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.manong.dao.business.BusinessTurnoverMapper;
import com.manong.entity.business.BusinessTurnover;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manong.service.business.BusinessTurnoverService;
import com.manong.vo.query.BusinessTurnoverQueryVo;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;


@Service
public class BusinessTurnoverServiceImpl extends ServiceImpl<BusinessTurnoverMapper, BusinessTurnover> implements BusinessTurnoverService {

    @Override
    public IPage<BusinessTurnover> findBusinessTurnoverListByName(IPage<BusinessTurnover> page, BusinessTurnoverQueryVo businessTurnoverQueryVo) {
        //创建条件构造器
        QueryWrapper<BusinessTurnover> queryWrapper = new QueryWrapper<BusinessTurnover>();
        //传感器编号
        queryWrapper.like(!ObjectUtils.isEmpty(businessTurnoverQueryVo.getTurnover()),"turnover",businessTurnoverQueryVo.getTurnover());
        //排序
        queryWrapper.orderByAsc("id");
        return baseMapper.selectPage(page,queryWrapper);
    }
}
