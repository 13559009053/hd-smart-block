package com.manong.service.business;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.manong.entity.business.BusinessWaterRent;
import com.manong.vo.query.BusinessWaterRentQueryVo;

public interface BusinessWaterRentService extends IService<BusinessWaterRent> {

    IPage<BusinessWaterRent> findBusinessWaterRentListByMoney(IPage<BusinessWaterRent> page, BusinessWaterRentQueryVo businessWaterRentQueryVo);
}
