package com.manong.service.business;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.manong.entity.business.Business;
import com.manong.vo.query.BusinessQueryVo;

public interface BusinessService extends IService<Business> {

    IPage<Business> findBusinessListByName(IPage<Business> page, BusinessQueryVo businessQueryVo);

    IPage<Business> findBusinessListByType(IPage<Business> page, BusinessQueryVo businessQueryVo);
}
