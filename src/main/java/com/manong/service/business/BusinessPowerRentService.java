package com.manong.service.business;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.manong.entity.business.BusinessPowerRent;
import com.manong.vo.query.BusinessPowerRentQueryVo;

public interface BusinessPowerRentService extends IService<BusinessPowerRent> {

    IPage<BusinessPowerRent> findBusinessPowerRentListByMoney(IPage<BusinessPowerRent> page, BusinessPowerRentQueryVo businessPowerRentQueryVo);
}
