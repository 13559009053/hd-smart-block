package com.manong.service.business;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.manong.entity.business.BusinessTurnover;
import com.manong.vo.query.BusinessTurnoverQueryVo;

public interface BusinessTurnoverService extends IService<BusinessTurnover> {

    IPage<BusinessTurnover> findBusinessTurnoverListByName(IPage<BusinessTurnover> page, BusinessTurnoverQueryVo businessTurnoverQueryVo);
}
