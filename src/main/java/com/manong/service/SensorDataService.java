package com.manong.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.manong.entity.SensorData;
import com.baomidou.mybatisplus.extension.service.IService;
import com.manong.vo.query.SensorDataQueryVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author czm
 * @since 2023-02-04
 */
public interface SensorDataService extends IService<SensorData> {

    SensorData findSensorDataBySensorId(Integer sensorId);

    int updateBySensorId(SensorData sensorId);

    IPage<SensorData> findSensorDataListBySensorId(IPage<SensorData> page, SensorDataQueryVo sensorDataQueryVo);
}
