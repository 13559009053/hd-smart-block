package com.manong.service.test.impl;

import com.alibaba.excel.EasyExcel;
import com.manong.entity.test.Test;
import com.manong.dao.test.TestMapper;
import com.manong.listener.ImportDataListener;
import com.manong.service.test.TestService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.InputStream;


@Service
public class TestServiceImpl extends ServiceImpl<TestMapper, Test> implements TestService {
    @Resource
    private ImportDataListener importDataListener;

    @SneakyThrows
    @Override
    public void importExcel(MultipartFile multipartFile) {
        InputStream inputStream = multipartFile.getInputStream();
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet 文件流会自动关闭
        EasyExcel.read(inputStream, Test.class, importDataListener).sheet().doRead();
    }
}
