package com.manong.service.test;

import com.manong.entity.test.Test;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author czm
 * @since 2023-03-22
 */
public interface TestService extends IService<Test> {

    void importExcel(MultipartFile multipartFile);
}
