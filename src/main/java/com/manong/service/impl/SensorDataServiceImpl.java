package com.manong.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.manong.entity.SensorData;
import com.manong.dao.SensorDataMapper;
import com.manong.entity.SensorInformation;
import com.manong.service.SensorDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manong.vo.query.SensorDataQueryVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author czm
 * @since 2023-02-04
 */
@Service
@Transactional
public class SensorDataServiceImpl extends ServiceImpl<SensorDataMapper, SensorData> implements SensorDataService {

    @Override
    public SensorData findSensorDataBySensorId(Integer sensorId) {
        return baseMapper.findSensorDataBySensorId(sensorId);
    }

    @Override
    public int updateBySensorId(SensorData sensorData) {
        return baseMapper.updateBySensorId(sensorData);
    }

    @Override
    public IPage<SensorData> findSensorDataListBySensorId(IPage<SensorData> page, SensorDataQueryVo sensorDataQueryVo) {
        //创建条件构造器
        QueryWrapper<SensorData> queryWrapper = new QueryWrapper<SensorData>();
        //传感器编号
        queryWrapper.eq(!ObjectUtils.isEmpty(sensorDataQueryVo.getSensorId()),"sensor_id",sensorDataQueryVo.getSensorId());
        //排序
        queryWrapper.orderByAsc("id");
        return baseMapper.selectPage(page,queryWrapper);
    }
}
