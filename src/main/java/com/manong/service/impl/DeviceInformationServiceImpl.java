package com.manong.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.manong.entity.DeviceInformation;
import com.manong.dao.DeviceInformationMapper;
import com.manong.service.DeviceInformationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manong.vo.query.DeviceInformationQueryVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;

/**
 * <p>
 * `device_information` 服务实现类
 * </p>
 *
 * @author czm
 * @since 2023-02-04
 */
@Service
@Transactional
public class DeviceInformationServiceImpl extends ServiceImpl<DeviceInformationMapper, DeviceInformation> implements DeviceInformationService {

    @Override
    public DeviceInformation findDeviceInformationByDeviceId(String deviceId) {
        return baseMapper.findDeviceInformationByDeviceId(deviceId);
    }

    @Override
    public ArrayList<DeviceInformation> findDeviceInformationByDeviceName(String deviceName) {
        return baseMapper.findDeviceInformationByDeviceName(deviceName);
    }

    @Override
    public IPage<DeviceInformation> findDeviceListByDeviceName(IPage<DeviceInformation> page, DeviceInformationQueryVo deviceInformationQueryVo) {
        //创建条件构造器
        QueryWrapper<DeviceInformation> queryWrapper = new QueryWrapper<DeviceInformation>();
        //设备名称
        queryWrapper.like(!ObjectUtils.isEmpty(deviceInformationQueryVo.getName()),"name",deviceInformationQueryVo.getName());
        //排序
        queryWrapper.orderByAsc("creat_time");
        return baseMapper.selectPage(page,queryWrapper);
    }

    @Override
    public boolean add(DeviceInformation deviceInformation) {
        return baseMapper.add(deviceInformation);
    }

    @Override
    public boolean updateDeviceInformationById(DeviceInformation deviceInformation) {
        return baseMapper.updateDeviceInformationById(deviceInformation);
    }

    @Override
    public boolean deleteDeviceInformationById(String id) {
        return baseMapper.deleteDeviceInformationById(id);
    }

}
