package com.manong.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.manong.entity.DeviceInformation;
import com.manong.entity.Role;
import com.manong.entity.SensorInformation;
import com.manong.dao.SensorInformationMapper;
import com.manong.service.SensorInformationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manong.vo.query.SensorInformationQueryVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;

/**
 * <p>
 * `sensor_information` 服务实现类
 * </p>
 *
 * @author czm
 * @since 2023-02-04
 */
@Service
@Transactional
public class SensorInformationServiceImpl extends ServiceImpl<SensorInformationMapper, SensorInformation> implements SensorInformationService {

    @Override
    public ArrayList<SensorInformation> findSensorInformationByName(String name) {
        return baseMapper.findSensorInformationByName(name);
    }

    @Override
    public ArrayList<SensorInformation> findSensorInformationByUserName(String userName) {
        return baseMapper.findSensorInformationByUserName(userName);
    }

    @Override
    public ArrayList<SensorInformation> findSensorInformationNoBind() {
        return baseMapper.findSensorInformationNoBind();
    }

    @Override
    public ArrayList<SensorInformation> findSensorInformationBindByUserName(String userName) {
        return baseMapper.findSensorInformationBindByUserName(userName);
    }

    @Override
    public int postSensorInformationCountByName(String name) {
        return baseMapper.postSensorInformationCountByName(name);
    }

    @Override
    public IPage<SensorInformation> findSensorListByName(IPage<SensorInformation> page, SensorInformationQueryVo sensorInformationQueryVo) {
        //创建条件构造器
        QueryWrapper<SensorInformation> queryWrapper = new QueryWrapper<SensorInformation>();
        //传感器名称
        queryWrapper.like(!ObjectUtils.isEmpty(sensorInformationQueryVo.getName()),"name",sensorInformationQueryVo.getName());
        //排序
        queryWrapper.orderByAsc("id");
        return baseMapper.selectPage(page,queryWrapper);
    }

    @Override
    public ArrayList<SensorInformation> findSensorInformationByDeviceId(String deviceId) {
        return baseMapper.findSensorInformationByDeviceId(deviceId);
    }

    @Override
    public ArrayList<SensorInformation> findSensorInformationById(Integer id) {
        return baseMapper.findSensorInformationById(id);
    }

    @Override
    public ArrayList<SensorInformation> findSensorInformationBindByUserNameSeven(String userName) {
        return baseMapper.findSensorInformationBindByUserNameSeven(userName);
    }

}
