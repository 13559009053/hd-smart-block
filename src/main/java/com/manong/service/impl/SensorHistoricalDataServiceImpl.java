package com.manong.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.manong.entity.SensorData;
import com.manong.entity.SensorHistoricalData;
import com.manong.dao.SensorHistoricalDataMapper;
import com.manong.entity.SensorInformation;
import com.manong.service.SensorHistoricalDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manong.vo.query.SensorHistoricalDataQueryVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;


@Service
@Transactional
public class SensorHistoricalDataServiceImpl extends ServiceImpl<SensorHistoricalDataMapper, SensorHistoricalData> implements SensorHistoricalDataService {

    @Override
    public ArrayList<SensorHistoricalData> findSensorHistoricalDataByDeviceId(String deviceId) {
        return baseMapper.findSensorHistoricalDataByDeviceId(deviceId);
    }

    @Override
    public IPage<SensorHistoricalData> findSensorHistoricalDataListBySensorId(IPage<SensorHistoricalData> page, SensorHistoricalDataQueryVo sensorHistoricalDataQueryVo) {
        //创建条件构造器
        QueryWrapper<SensorHistoricalData> queryWrapper = new QueryWrapper<SensorHistoricalData>();
        //传感器编号
        queryWrapper.like(!ObjectUtils.isEmpty(sensorHistoricalDataQueryVo.getDeviceId()),"device_id",sensorHistoricalDataQueryVo.getDeviceId());
        //排序
        queryWrapper.orderByAsc("id");
        return baseMapper.selectPage(page,queryWrapper);
    }

    @Override
    public ArrayList<SensorHistoricalData> findSensorHistoricalDataByDeviceIdSixteen(String deviceId) {
        return baseMapper.findSensorHistoricalDataByDeviceIdSixteen(deviceId);
    }

    @Override
    public int findUnusualNumber() {
        return baseMapper.findUnusualNumber();
    }

    @Override
    public int findUnusualNumberHumidity() {
        return baseMapper.findUnusualNumberHumidity();
    }

    @Override
    public int findUnusualNumberIllumination() {
        return baseMapper.findUnusualNumberIllumination();
    }
}
