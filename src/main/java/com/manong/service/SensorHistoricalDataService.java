package com.manong.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.manong.entity.SensorHistoricalData;
import com.baomidou.mybatisplus.extension.service.IService;
import com.manong.entity.SensorInformation;
import com.manong.vo.query.SensorHistoricalDataQueryVo;

import java.util.ArrayList;


public interface SensorHistoricalDataService extends IService<SensorHistoricalData> {

    ArrayList<SensorHistoricalData> findSensorHistoricalDataByDeviceId(String deviceId);

    IPage<SensorHistoricalData>  findSensorHistoricalDataListBySensorId(IPage<SensorHistoricalData> page, SensorHistoricalDataQueryVo sensorHistoricalDataQueryVo);

    ArrayList<SensorHistoricalData> findSensorHistoricalDataByDeviceIdSixteen(String deviceId);

    int findUnusualNumber();

    int findUnusualNumberHumidity();

    int findUnusualNumberIllumination();
}
