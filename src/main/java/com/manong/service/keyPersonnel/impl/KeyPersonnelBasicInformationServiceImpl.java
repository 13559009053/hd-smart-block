package com.manong.service.keyPersonnel.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.manong.entity.keyPersonnel.KeyPersonnelBasicInformation;
import com.manong.dao.keyPersonnel.KeyPersonnelBasicInformationMapper;
import com.manong.service.keyPersonnel.KeyPersonnelBasicInformationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manong.vo.query.keypersonnel.KeyPersonnelQueryVo;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
public class KeyPersonnelBasicInformationServiceImpl extends ServiceImpl<KeyPersonnelBasicInformationMapper, KeyPersonnelBasicInformation> implements KeyPersonnelBasicInformationService {

    @Override
    public IPage<KeyPersonnelBasicInformation> findKeyPersonnelListByName(IPage<KeyPersonnelBasicInformation> page, KeyPersonnelQueryVo keyPersonnelQueryVo) {
        //创建条件构造器
        QueryWrapper<KeyPersonnelBasicInformation> queryWrapper = new QueryWrapper<KeyPersonnelBasicInformation>();
        //重点人员
        queryWrapper.like(!ObjectUtils.isEmpty(keyPersonnelQueryVo.getName()),"name",keyPersonnelQueryVo.getName());
        //排序
        queryWrapper.orderByAsc("id");
        return baseMapper.selectPage(page,queryWrapper);
    }

    @Override
    public IPage<KeyPersonnelBasicInformation> findKeyPersonnelByHazardDegree(IPage<KeyPersonnelBasicInformation> page, KeyPersonnelQueryVo keyPersonnelQueryVo) {
        //创建条件构造器
        QueryWrapper<KeyPersonnelBasicInformation> queryWrapper = new QueryWrapper<KeyPersonnelBasicInformation>();
        //重点人员
        queryWrapper.eq("hazard_degree",keyPersonnelQueryVo.getHazardDegree());
        //排序
        queryWrapper.orderByAsc("id");
        return baseMapper.selectPage(page,queryWrapper);
    }
}
