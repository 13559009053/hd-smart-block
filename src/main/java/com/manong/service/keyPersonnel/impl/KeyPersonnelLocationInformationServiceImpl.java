package com.manong.service.keyPersonnel.impl;

import com.manong.dao.keyPersonnel.KeyPersonnelLocationInformationMapper;
import com.manong.entity.keyPersonnel.KeyPersonnelLocationInformation;
import com.manong.service.keyPersonnel.KeyPersonnelLocationInformationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class KeyPersonnelLocationInformationServiceImpl extends ServiceImpl<KeyPersonnelLocationInformationMapper, KeyPersonnelLocationInformation> implements KeyPersonnelLocationInformationService {

    @Override
    public KeyPersonnelLocationInformation findKeyPersonnelLocationInformationByPersonnelId(Integer personnelId) {
        return baseMapper.findKeyPersonnelLocationInformationByPersonnelId(personnelId);
    }
}
