package com.manong.service.keyPersonnel;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.manong.entity.keyPersonnel.KeyPersonnelBasicInformation;
import com.manong.vo.query.keypersonnel.KeyPersonnelQueryVo;

public interface KeyPersonnelBasicInformationService extends IService<KeyPersonnelBasicInformation> {

    IPage<KeyPersonnelBasicInformation> findKeyPersonnelListByName(IPage<KeyPersonnelBasicInformation> page, KeyPersonnelQueryVo keyPersonnelQueryVo);

    IPage<KeyPersonnelBasicInformation> findKeyPersonnelByHazardDegree(IPage<KeyPersonnelBasicInformation> page, KeyPersonnelQueryVo keyPersonnelQueryVo);
}
