package com.manong.service.keyPersonnel;

import com.baomidou.mybatisplus.extension.service.IService;
import com.manong.entity.keyPersonnel.KeyPersonnelLocationInformation;

public interface KeyPersonnelLocationInformationService extends IService<KeyPersonnelLocationInformation> {

    KeyPersonnelLocationInformation findKeyPersonnelLocationInformationByPersonnelId(Integer personnelId);
}
