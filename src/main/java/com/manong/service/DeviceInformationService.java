package com.manong.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.manong.entity.DeviceInformation;
import com.baomidou.mybatisplus.extension.service.IService;
import com.manong.vo.query.DeviceInformationQueryVo;

import java.util.ArrayList;

/**
 * <p>
 * `device_information` 服务类
 * </p>
 *
 * @author czm
 * @since 2023-02-04
 */
public interface DeviceInformationService extends IService<DeviceInformation> {

    DeviceInformation findDeviceInformationByDeviceId(String deviceId);

    ArrayList<DeviceInformation> findDeviceInformationByDeviceName(String deviceName);

    IPage<DeviceInformation>  findDeviceListByDeviceName(IPage<DeviceInformation> page, DeviceInformationQueryVo deviceQueryVo);

    boolean add(DeviceInformation deviceInformation);

    boolean updateDeviceInformationById(DeviceInformation deviceInformation);

    boolean deleteDeviceInformationById(String id);
}
