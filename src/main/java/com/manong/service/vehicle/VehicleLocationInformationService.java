package com.manong.service.vehicle;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.manong.entity.vehicle.VehicleLocationInformation;
import com.baomidou.mybatisplus.extension.service.IService;
import com.manong.vo.query.vehicle.VehicleLocationInformationQueryVo;

public interface VehicleLocationInformationService extends IService<VehicleLocationInformation> {

    IPage<VehicleLocationInformation> findVehicleLocationInformationByCarNumber(IPage<VehicleLocationInformation> page, VehicleLocationInformationQueryVo vehicleLocationInformationQueryVo);
}
