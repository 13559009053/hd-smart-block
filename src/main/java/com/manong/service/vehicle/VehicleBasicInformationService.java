package com.manong.service.vehicle;

import com.manong.entity.vehicle.VehicleBasicInformation;
import com.baomidou.mybatisplus.extension.service.IService;

public interface VehicleBasicInformationService extends IService<VehicleBasicInformation> {

    VehicleBasicInformation findVehicleBasicInformationByCarNumber(String carNumber);
}
