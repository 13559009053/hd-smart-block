package com.manong.service.vehicle;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.manong.entity.vehicle.VehicleUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.manong.vo.query.vehicle.VehicleUserQueryVo;

public interface VehicleUserService extends IService<VehicleUser> {

    IPage<VehicleUser> findVehicleUserByName(IPage<VehicleUser> page, VehicleUserQueryVo vehicleUserQueryVo);

    IPage<VehicleUser> findVehicleUserBySex(IPage<VehicleUser> page, VehicleUserQueryVo vehicleUserQueryVo);
}
