package com.manong.service.vehicle.impl;

import com.manong.entity.vehicle.VehicleBasicInformation;
import com.manong.dao.vehicle.VehicleBasicInformationMapper;
import com.manong.service.vehicle.VehicleBasicInformationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class VehicleBasicInformationServiceImpl extends ServiceImpl<VehicleBasicInformationMapper, VehicleBasicInformation> implements VehicleBasicInformationService {

    @Override
    public VehicleBasicInformation findVehicleBasicInformationByCarNumber(String carNumber) {
        return baseMapper.findVehicleBasicInformationByCarNumber(carNumber);
    }
}
