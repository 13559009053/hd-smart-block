package com.manong.service.vehicle.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.manong.entity.vehicle.VehicleUser;
import com.manong.dao.vehicle.VehicleUserMapper;
import com.manong.service.vehicle.VehicleUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manong.vo.query.vehicle.VehicleUserQueryVo;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
public class VehicleUserServiceImpl extends ServiceImpl<VehicleUserMapper, VehicleUser> implements VehicleUserService {

    @Override
    public IPage<VehicleUser> findVehicleUserByName(IPage<VehicleUser> page, VehicleUserQueryVo vehicleUserQueryVo) {
        //创建条件构造器
        QueryWrapper<VehicleUser> queryWrapper = new QueryWrapper<VehicleUser>();
        //车主
        queryWrapper.like(!ObjectUtils.isEmpty(vehicleUserQueryVo.getName()),"name",vehicleUserQueryVo.getName());
        //排序
        queryWrapper.orderByAsc("id");
        return baseMapper.selectPage(page,queryWrapper);
    }

    @Override
    public IPage<VehicleUser> findVehicleUserBySex(IPage<VehicleUser> page, VehicleUserQueryVo vehicleUserQueryVo) {
        //创建条件构造器
        QueryWrapper<VehicleUser> queryWrapper = new QueryWrapper<VehicleUser>();
        //车主
        queryWrapper.eq("sex",vehicleUserQueryVo.getSex());
        //排序
        queryWrapper.orderByAsc("id");
        return baseMapper.selectPage(page,queryWrapper);
    }
}
