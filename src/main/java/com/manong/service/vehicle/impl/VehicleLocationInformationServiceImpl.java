package com.manong.service.vehicle.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.manong.entity.vehicle.VehicleLocationInformation;
import com.manong.dao.vehicle.VehicleLocationInformationMapper;
import com.manong.service.vehicle.VehicleLocationInformationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manong.vo.query.vehicle.VehicleLocationInformationQueryVo;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
public class VehicleLocationInformationServiceImpl extends ServiceImpl<VehicleLocationInformationMapper, VehicleLocationInformation> implements VehicleLocationInformationService {

    @Override
    public IPage<VehicleLocationInformation> findVehicleLocationInformationByCarNumber(IPage<VehicleLocationInformation> page, VehicleLocationInformationQueryVo vehicleLocationInformationQueryVo) {
        //创建条件构造器
        QueryWrapper<VehicleLocationInformation> queryWrapper = new QueryWrapper<VehicleLocationInformation>();
        //车辆位置
        queryWrapper.like(!ObjectUtils.isEmpty(vehicleLocationInformationQueryVo.getCarNumber()),"car_number",vehicleLocationInformationQueryVo.getCarNumber());
        //排序
        queryWrapper.orderByAsc("id");
        return baseMapper.selectPage(page,queryWrapper);
    }
}
