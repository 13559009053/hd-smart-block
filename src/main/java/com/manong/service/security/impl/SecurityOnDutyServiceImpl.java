package com.manong.service.security.impl;

import com.manong.entity.security.SecurityOnDuty;
import com.manong.dao.security.SecurityOnDutyMapper;
import com.manong.service.security.SecurityOnDutyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class SecurityOnDutyServiceImpl extends ServiceImpl<SecurityOnDutyMapper, SecurityOnDuty> implements SecurityOnDutyService {

    @Override
    public SecurityOnDuty findSecurityOnDutyById(Integer dutyId) {
        return baseMapper.findSecurityOnDutyById(dutyId);
    }
}
