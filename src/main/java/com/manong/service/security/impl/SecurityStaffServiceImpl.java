package com.manong.service.security.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.manong.dao.security.SecurityStaffMapper;
import com.manong.entity.security.SecurityStaff;
import com.manong.service.security.SecurityStaffService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.manong.vo.query.security.SecurityStaffQueryVo;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
public class SecurityStaffServiceImpl extends ServiceImpl<SecurityStaffMapper, SecurityStaff> implements SecurityStaffService {

    @Override
    public IPage<SecurityStaff> findSecurityStaffListByName(IPage<SecurityStaff> page, SecurityStaffQueryVo securityStaffQueryVo) {
        //创建条件构造器
        QueryWrapper<SecurityStaff> queryWrapper = new QueryWrapper<SecurityStaff>();
        //水费
        queryWrapper.like(!ObjectUtils.isEmpty(securityStaffQueryVo.getName()),"name",securityStaffQueryVo.getName());
        //排序
        queryWrapper.orderByAsc("id");
        return baseMapper.selectPage(page,queryWrapper);
    }
}
