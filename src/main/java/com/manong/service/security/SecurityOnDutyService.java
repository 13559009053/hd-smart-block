package com.manong.service.security;

import com.baomidou.mybatisplus.extension.service.IService;
import com.manong.entity.security.SecurityOnDuty;

public interface SecurityOnDutyService extends IService<SecurityOnDuty> {

    SecurityOnDuty findSecurityOnDutyById(Integer dutyId);
}
