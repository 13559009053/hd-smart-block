package com.manong.service.security;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.manong.entity.security.SecurityStaff;
import com.manong.vo.query.security.SecurityStaffQueryVo;

public interface SecurityStaffService extends IService<SecurityStaff> {

    IPage<SecurityStaff> findSecurityStaffListByName(IPage<SecurityStaff> page, SecurityStaffQueryVo securityStaffQueryVo);
}
