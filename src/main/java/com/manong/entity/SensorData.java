package com.manong.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author czm
 * @since 2023-02-04
 */
@Data
@TableName("sys_sensor_data")
public class SensorData implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 实时数据编号
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 温度
     */
    private Double temperature;

    /**
     * 湿度
     */
    private Double humidity;

    /**
     * 光照
     */
    private Double illumination;

    /**
     * 照片
     */
    private String photo;

    /**
     * 视频
     */
    private String video;

    /**
     * sensor基础表主键
     */
    private Integer sensorId;


}
