package com.manong.entity.security;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_security_on_duty")
public class SecurityOnDuty implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 值班编号
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 星期几(1代表星期一,2代表星期二)
     */
    private Integer week;

    /**
     * 值班开始时间
     */
    private Date startTime;

    /**
     * 值班结束时间
     */
    private Date endTime;


}
