package com.manong.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * `sensor_information`
 * </p>
 *
 * @author czm
 * @since 2023-02-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_sensor_information")
public class SensorInformation implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 数据编号
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 数据名称
     */
    private String name;

    /**
     * 获取数据时间
     */
    private Date time;

    private Double lng;

    private Double lat;

    /**
     * 用户名
     */
    private String userName;
    /**
     * 设备编号
     */
    private String deviceId;


}
