package com.manong.entity.vehicle;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * `vehicle_location_information`
 * </p>
 *
 * @author czm
 * @since 2023-02-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_vehicle_location_information")
public class VehicleLocationInformation implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 车辆位置信息编号
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 经度
     */
    private Integer lng;

    /**
     * 纬度
     */
    private Integer lat;

    /**
     * 抓拍照片地址
     */
    private String takePhotos;

    /**
     * 抓拍视频地址
     */
    private String video;

    /**
     * 抓拍时间
     */
    private LocalDateTime takeTime;

    /**
     * 类型(0陌生车辆1商家车辆2保安车辆)
     */
    private Integer type;

    /**
     * 车牌号
     */
    private String carNumber;


}
