package com.manong.entity.vehicle;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * `vehicle_basic_information`
 * </p>
 *
 * @author czm
 * @since 2023-02-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_vehicle_basic_information")
public class VehicleBasicInformation implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 车牌号
     */
      @TableId(value = "car_number", type = IdType.AUTO)
    private String carNumber;

    /**
     * 车照片
     */
    private String carPhoto;


}
