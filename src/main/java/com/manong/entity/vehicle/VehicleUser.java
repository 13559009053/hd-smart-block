package com.manong.entity.vehicle;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_vehicle_user")
public class VehicleUser implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 车主编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 身份证号
     */
    private Long idNumber;

    /**
     * 车主名字
     */
    private String name;

    /**
     * 车主照片地址
     */
    private String photo;

    /**
     * 性别
     */
    private Integer sex;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 手机号
     */
    private Long phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 地址
     */
    private String address;

    /**
     * 车牌号
     */
    private String carNumber;


}
