package com.manong.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.Date;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.stereotype.Component;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_sensor_historical_data")
@Component
public class SensorHistoricalData implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 历史数据id
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 温度
     */
    @SerializedName(value = "temperature", alternate = "temp")
    private Double temperature;

    /**
     * 湿度
     */
    @SerializedName(value = "humidity", alternate = "humi")
    private Double humidity;

    /**
     * 光敏
     */
    @SerializedName("light")
    private Double illumination;

    /**
     * 时间
     */
    @SerializedName(value = "createtime", alternate = {"time","ctime"})
    private Date time;

    /**
     * 设备id
     */
    @SerializedName("deviceid")
    private String deviceId;


}
