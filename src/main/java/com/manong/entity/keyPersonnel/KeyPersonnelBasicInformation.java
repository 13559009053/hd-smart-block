package com.manong.entity.keyPersonnel;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_key_personnel_basic_information")
public class KeyPersonnelBasicInformation implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 重点人员编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 身份证号
     */
    private Long idNumber;

    /**
     * 姓名
     */
    private String name;

    /**
     * 性别(0女1男)
     */
    private Integer sex;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 重点人员照片
     */
    private String photo;

    /**
     * 危险级别
     */
    private String hazardDegree;


}
