package com.manong.entity.keyPersonnel;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_key_personnel_location_information")
public class KeyPersonnelLocationInformation implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 位置信息编号
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 经度
     */
    private Double lng;

    /**
     * 纬度
     */
    private Double lat;

    /**
     * 抓拍照片地址
     */
    private String takePhotos;

    /**
     * 抓拍视频地址
     */
    private String video;

    /**
     * 抓拍时间
     */
    private LocalDateTime takeTime;

    /**
     * 0重点人员，1摔倒，2聚众，3打架，4落水
     */
    private Integer type;

    /**
     * 重点人员编号
     */
    private Integer personnelId;


}
