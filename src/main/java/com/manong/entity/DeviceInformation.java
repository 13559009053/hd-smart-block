package com.manong.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;

@Data
@TableName("sys_device_information")
public class DeviceInformation implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 设备id编号
     */
      @TableId(value = "id", type = IdType.AUTO)
    private String id;

    /**
     * 设备名字
     */
    private String name;

    /**
     * 经度
     */
    private Double lng;

    /**
     * 纬度
     */
    private Double lat;

    /**
     * 创建时间
     */
    private String creatTime;


}
