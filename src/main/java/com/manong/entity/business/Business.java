package com.manong.entity.business;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 商家
 * </p>
 *
 * @author czm
 * @since 2023-02-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_business")
public class Business implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 商家编号
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商家名
     */
    private String name;

    /**
     * 商家类型(0吃、1喝、2穿、3玩)
     */
    private Integer type;

    /**
     * 经度
     */
    private Float lng;

    /**
     * 纬度
     */
    private Float lat;

    /**
     * 楼层数
     */
    private Integer layer;

    /**
     * 面积
     */
    private Integer area;

    /**
     * 用户id
     */
    private Long userId;


}
