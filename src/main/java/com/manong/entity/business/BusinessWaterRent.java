package com.manong.entity.business;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_business_water_rent")
public class BusinessWaterRent implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 水费编号
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 水费
     */
    private Integer money;

    /**
     * 支付时间
     */
    private Date payDate;

    /**
     * 商家编号
     */
    private Integer businessId;


}
