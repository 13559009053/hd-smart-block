package com.manong.entity.business;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * `business_power_rent`
 * </p>
 *
 * @author czm
 * @since 2023-02-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_business_power_rent")
public class BusinessPowerRent implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 电费编号
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 电费
     */
    private Integer money;

    /**
     * 支付时间
     */
    private Date payDate;

    /**
     * 商家编号
     */
    private Integer businessId;


}
