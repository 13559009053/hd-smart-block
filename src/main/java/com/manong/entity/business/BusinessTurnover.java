package com.manong.entity.business;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_business_turnover")
public class BusinessTurnover implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 营业额编号
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 营业额
     */
    private Integer turnover;

    /**
     * 营业时间
     */
    @com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd")
    private Date date;

    /**
     * 商家编号
     */
    private Integer businessId;


}
