package com.manong.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.manong.entity.SensorData;
import com.manong.entity.SensorHistoricalData;
import com.manong.service.SensorHistoricalDataService;
import com.manong.utils.Result;
import com.manong.vo.query.SensorDataQueryVo;
import com.manong.vo.query.SensorHistoricalDataQueryVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;

@RestController
@RequestMapping("/api/sensorHistoricalData")
public class SensorHistoricalDataController {
    @Resource
    private SensorHistoricalDataService sensorHistoricalDataService;

    /**
     * 通过deviceId查询历史数据的16条记录
     */
    @PostMapping("/postSensorHistoricalDataByDeviceIdSixteen")
    public Result postSensorHistoricalDataByDeviceIdSixteen(@RequestBody SensorHistoricalData sensorHistoricalData){
        ArrayList<SensorHistoricalData> list=sensorHistoricalDataService.findSensorHistoricalDataByDeviceIdSixteen(sensorHistoricalData.getDeviceId());
        return Result.ok(list);
    }
    /**
     * 通过deviceId查询历史数据的7条记录
     */
    @PostMapping("/postSensorHistoricalDataByDeviceId")
    public Result postSensorHistoricalDataByDeviceId(@RequestBody SensorHistoricalData sensorHistoricalData){
        ArrayList<SensorHistoricalData> list=sensorHistoricalDataService.findSensorHistoricalDataByDeviceId(sensorHistoricalData.getDeviceId());
        return Result.ok(list);
    }
    /**
     * 查询湿度异常硬件大于100的记录数
     */
    @GetMapping("/unusualNumberHumidity")
    public Result unusualNumberHumidity(){
        int n=sensorHistoricalDataService.findUnusualNumberHumidity();
        //返回数据
        return Result.ok(n);
    }
    /**
     * 查询光敏异常硬件大于200的记录数
     */
    @GetMapping("/unusualNumberIllumination")
    public Result unusualNumberIllumination(){
        int n=sensorHistoricalDataService.findUnusualNumberIllumination();
        //返回数据
        return Result.ok(n);
    }
    /**
     * 查询温度异常硬件大于25的记录数
     */
    @GetMapping("/unusualNumber")
    public Result unusualNumber(){
        int n=sensorHistoricalDataService.findUnusualNumber();
        //返回数据
        return Result.ok(n);
    }
    /**
     * 查询总记录
     */
    @GetMapping("/totalRecords")
    public Result totalRecords(){
        int n=sensorHistoricalDataService.count();
        //返回数据
        return Result.ok(n);
    }
    /**
     * 分页查询历史数据
     */
    @GetMapping("/list")
    public Result list(SensorHistoricalDataQueryVo sensorHistoricalDataQueryVo){
        //创建分页对象
        IPage<SensorHistoricalData> page = new Page<SensorHistoricalData>(sensorHistoricalDataQueryVo.getPageNo(),sensorHistoricalDataQueryVo.getPageSize());
        //调用分页查询方法
        sensorHistoricalDataService.findSensorHistoricalDataListBySensorId(page,sensorHistoricalDataQueryVo);
        //返回数据
        return Result.ok(page);
    }
    /**
     * 添加历史数据
     */
    @PostMapping("/add")
    public Result add(@RequestBody SensorHistoricalData sensorHistoricalData){
        if (sensorHistoricalDataService.save(sensorHistoricalData)){
            return Result.ok().message("硬件添加成功");
        }
        return Result.error().message("硬件添加失败");
    }
    /**
     * 修改删除数据
     */
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable Integer id){
        if (sensorHistoricalDataService.removeById(id)){
            return Result.ok().message("历史数据删除成功");
        }else {
            return Result.error().message("历史数据删除失败");
        }
    }
}

