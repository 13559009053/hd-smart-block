package com.manong.controller.test;

import com.manong.service.test.TestService;
import com.manong.utils.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

@Slf4j
@RestController
@RequestMapping("/api/test")
public class TestController {
    @Resource
    private TestService testService;

    @PostMapping("/import")
    public Result importExcel(@RequestBody MultipartFile multipartFile){
        testService.importExcel(multipartFile);
        return Result.ok().message("导入成功");
    }
}

