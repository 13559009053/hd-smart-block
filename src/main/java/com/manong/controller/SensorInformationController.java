package com.manong.controller;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.manong.entity.DeviceInformation;
import com.manong.entity.SensorInformation;
import com.manong.service.DeviceInformationService;
import com.manong.service.SensorDataService;
import com.manong.service.SensorInformationService;
import com.manong.utils.Result;
import com.manong.vo.query.SensorInformationQueryVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;

@RestController
@RequestMapping("/api/sensorInformation")
public class SensorInformationController {
    @Resource
    private SensorInformationService sensorInformationService;
    //绑定表业务层注入
    @Resource
    private SensorDataService sensorDataService;
    @Resource
    private DeviceInformationService deviceInformationService;
    /**
     * 查询所有硬件基本信息
     */
    @GetMapping("/listAll")
    public Result listAll(){
        return Result.ok(sensorInformationService.list());
    }
    /**
     * 查询没绑定过的硬件
     */
    @GetMapping("/getSensorInformationNoBind")
    public Result getSensorInformationNoBind(){
        ArrayList<SensorInformation> list = sensorInformationService.findSensorInformationNoBind();
        return Result.ok(list);
    }
    /**
     * 查询绑定过的硬件
     */
    @PostMapping("/postSensorInformationBindByUserName")
    public Result postSensorInformationBindByUserName(@RequestBody SensorInformation sensorInformation){
        ArrayList<SensorInformation> list = sensorInformationService.findSensorInformationBindByUserName(sensorInformation.getUserName());
        return Result.ok(list);
    }
    /**
     * 查询绑定过的硬件7条记录
     */
    @PostMapping("/postSensorInformationBindByUserNameSeven")
    public Result postSensorInformationBindByUserNameSeven(@RequestBody SensorInformation sensorInformation){
        ArrayList<SensorInformation> list = sensorInformationService.findSensorInformationBindByUserNameSeven(sensorInformation.getUserName());
        return Result.ok(list);
    }
    /**
     * 通过设备Id查询所有硬件基本信息
     */
    @GetMapping("/getSensorInformationByDeviceId")
    public Result getSensorInformationByDeviceId(@PathVariable String deviceId){
        ArrayList<SensorInformation> list=sensorInformationService.findSensorInformationByDeviceId(deviceId);
        return Result.ok(list);
    }
    /**
     * 通过Id查询所有硬件基本信息
     */
    @GetMapping("/getSensorInformationById")
    public Result getSensorInformationById(@PathVariable Integer id){
        ArrayList<SensorInformation> list=sensorInformationService.findSensorInformationById(id);
        return Result.ok(list);
    }
    /**
     * 通过姓名查询所有硬件基本信息
     */
    @PostMapping("/postSensorInformationByName")
    public Result postSensorInformationByName(@RequestBody SensorInformation sensorInformation){
        ArrayList<SensorInformation> list=sensorInformationService.findSensorInformationByName(sensorInformation.getName());
        return Result.ok(list);
    }
    /**
     * 通过姓名查询硬件个数
     */
    @PostMapping("/postSensorInformationCountByName")
    public Result postSensorInformationCountByName(@RequestBody SensorInformation sensorInformation){
        int n=sensorInformationService.postSensorInformationCountByName(sensorInformation.getName());
        return Result.ok(n);
    }
    /**
     * 通过用户名查询5条用户操作的基本信息
     */
    @PostMapping("/postSensorInformationByUserName")
    public Result postSensorInformationByUserName(@RequestBody SensorInformation sensorInformation){
        ArrayList<SensorInformation> list=sensorInformationService.findSensorInformationByUserName(sensorInformation.getUserName());
        return Result.ok(list);
    }
    /**
     * 分页查询传感器
     */
    @GetMapping("/list")
    public Result list(SensorInformationQueryVo sensorInformationQueryVo){
        //创建分页对象
        IPage<SensorInformation> page = new Page<SensorInformation>(sensorInformationQueryVo.getPageNo(),sensorInformationQueryVo.getPageSize());
        //调用分页查询方法
        sensorInformationService.findSensorListByName(page,sensorInformationQueryVo);
        //返回数据
        return Result.ok(page);
    }
    /**
     * 添加传感器基本信息
     */
    @PostMapping("/add")
    public Result add(@RequestBody SensorInformation sensorInformation){
        if(deviceInformationService.findDeviceInformationByDeviceId(sensorInformation.getDeviceId())!=null){
            if (sensorInformationService.save(sensorInformation)){
                return Result.ok().message("传感器添加成功");
            }
            return Result.error().message("传感器添加失败");
        }else {
            return Result.error().message("传感器添加失败无该设备编号");
        }
    }
    /**
     * 修改传感器信息
     */
    @PutMapping("/update")
    public Result update(@RequestBody SensorInformation sensorInformation){
        if(sensorInformationService.updateById(sensorInformation)){
            return Result.ok().message("传感器修改成功");
        }
        return Result.error().message("传感器修改失败");
    }
    /**
     * 删除传感器信息
     */
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable Integer id){
        /**
         * 先判断传感器基本信息表有没有被绑定
         */
        if(sensorDataService.findSensorDataBySensorId(id)!=null){
            return Result.error().message("传感器被绑定删除失败先删除子表");
        }else {
            if (sensorInformationService.removeById(id)){
                return Result.ok().message("传感器删除成功");
            }else {
                return Result.error().message("传感器删除失败");
            }
        }
    }
}

