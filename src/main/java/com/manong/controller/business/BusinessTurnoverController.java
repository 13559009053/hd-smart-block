package com.manong.controller.business;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.manong.entity.business.Business;
import com.manong.entity.business.BusinessTurnover;
import com.manong.service.business.BusinessTurnoverService;
import com.manong.utils.Result;
import com.manong.vo.query.BusinessTurnoverQueryVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/businessTurnover")
public class BusinessTurnoverController {
    @Resource
    private BusinessTurnoverService businessTurnoverService;
    /**
     * 分页查询商店列表
     */
    @GetMapping("/list")
    public Result list(BusinessTurnoverQueryVo businessTurnoverQueryVo){
        //创建分页对象
        IPage<BusinessTurnover> page = new Page<BusinessTurnover>(businessTurnoverQueryVo.getPageNo(),businessTurnoverQueryVo.getPageSize());
        //调用分页查询方法
        businessTurnoverService.findBusinessTurnoverListByName(page,businessTurnoverQueryVo);
        //返回数据
        return Result.ok(page);
    }
    /**
     * 添加商家
     */
    @PostMapping("/add")
    public Result add(@RequestBody BusinessTurnover businessTurnover){
        if (businessTurnoverService.save(businessTurnover)){
            return Result.ok().message("商家营业额添加成功");
        }
        return Result.error().message("商家营业额添加失败");
    }
    /**
     * 修改商家
     */
    @PutMapping("/update")
    public Result update(@RequestBody BusinessTurnover businessTurnover){
        if (businessTurnoverService.updateById(businessTurnover)) {
            return Result.ok().message("商家营业额修改成功");
        }else {
            return Result.error().message("商家营业额修改失败");
        }
    }
    /**
     * 删除实时数据
     */
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable Integer id){
        if (businessTurnoverService.removeById(id)){
            return Result.ok().message("商家营业额删除成功");
        }else {
            return Result.error().message("商家营业额删除失败");
        }
    }
}

