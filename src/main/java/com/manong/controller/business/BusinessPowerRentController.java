package com.manong.controller.business;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.manong.entity.business.BusinessPowerRent;
import com.manong.service.business.BusinessPowerRentService;
import com.manong.utils.Result;
import com.manong.vo.query.BusinessPowerRentQueryVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/businessPowerRent")
public class BusinessPowerRentController {
    @Resource
    private BusinessPowerRentService businessPowerRentService;
    /**
     * 分页查询商店列表
     */
    @GetMapping("/list")
    public Result list(BusinessPowerRentQueryVo businessPowerRentQueryVo){
        //创建分页对象
        IPage<BusinessPowerRent> page = new Page<BusinessPowerRent>(businessPowerRentQueryVo.getPageNo(),businessPowerRentQueryVo.getPageSize());
        //调用分页查询方法
        businessPowerRentService.findBusinessPowerRentListByMoney(page,businessPowerRentQueryVo);
        //返回数据
        return Result.ok(page);
    }
    /**
     * 添加商家
     */
    @PostMapping("/add")
    public Result add(@RequestBody BusinessPowerRent businessPowerRent){
        if (businessPowerRentService.save(businessPowerRent)){
            return Result.ok().message("商家电费表添加成功");
        }
        return Result.error().message("商家电费表添加失败");
    }
    /**
     * 修改商家
     */
    @PutMapping("/update")
    public Result update(@RequestBody BusinessPowerRent businessPowerRent){
        if (businessPowerRentService.updateById(businessPowerRent)) {
            return Result.ok().message("商家电费表修改成功");
        }else {
            return Result.error().message("商家电费表修改失败");
        }
    }
    /**
     * 删除实时数据
     */
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable Integer id){
        if (businessPowerRentService.removeById(id)){
            return Result.ok().message("商家电费表删除成功");
        }else {
            return Result.error().message("商家电费表删除失败");
        }
    }
}

