package com.manong.controller.business;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.manong.entity.business.BusinessWaterRent;
import com.manong.service.business.BusinessWaterRentService;
import com.manong.utils.Result;
import com.manong.vo.query.BusinessWaterRentQueryVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/businessWaterRent")
public class BusinessWaterRentController {
    @Resource
    private BusinessWaterRentService businessWaterRentService;
    /**
     * 分页查询商店列表
     */
    @GetMapping("/list")
    public Result list(BusinessWaterRentQueryVo businessWaterRentQueryVo){
        //创建分页对象
        IPage<BusinessWaterRent> page = new Page<BusinessWaterRent>(businessWaterRentQueryVo.getPageNo(),businessWaterRentQueryVo.getPageSize());
        //调用分页查询方法
        businessWaterRentService.findBusinessWaterRentListByMoney(page,businessWaterRentQueryVo);
        //返回数据
        return Result.ok(page);
    }
    /**
     * 添加商家
     */
    @PostMapping("/add")
    public Result add(@RequestBody BusinessWaterRent businessWaterRent){
        if (businessWaterRentService.save(businessWaterRent)){
            return Result.ok().message("商家水费表添加成功");
        }
        return Result.error().message("商家水费表添加失败");
    }
    /**
     * 修改商家
     */
    @PutMapping("/update")
    public Result update(@RequestBody BusinessWaterRent businessWaterRent){
        if (businessWaterRentService.updateById(businessWaterRent)) {
            return Result.ok().message("商家水费表修改成功");
        }else {
            return Result.error().message("商家水费表修改失败");
        }
    }
    /**
     * 删除实时数据
     */
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable Integer id){
        if (businessWaterRentService.removeById(id)){
            return Result.ok().message("商家水费表删除成功");
        }else {
            return Result.error().message("商家水费表删除失败");
        }
    }
}

