package com.manong.controller.business;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.manong.entity.business.Business;
import com.manong.service.business.BusinessService;
import com.manong.utils.Result;
import com.manong.vo.query.BusinessQueryVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/api/business")
public class BusinessController {
    @Resource
    private BusinessService businessService;
    /**
     * 通过type分页查询商店列表
     */
    @GetMapping("/typeList")
    public Result typeList(BusinessQueryVo businessQueryVo){
        //创建分页对象
        IPage<Business> page = new Page<Business>(businessQueryVo.getPageNo(),businessQueryVo.getPageSize());
        //调用分页查询方法
        businessService.findBusinessListByType(page,businessQueryVo);
        //返回数据
        return Result.ok(page);
    }
    /**
     * 分页查询商店列表
     */
    @GetMapping("/list")
    public Result list(BusinessQueryVo businessQueryVo){
        //创建分页对象
        IPage<Business> page = new Page<Business>(businessQueryVo.getPageNo(),businessQueryVo.getPageSize());
        //调用分页查询方法
        businessService.findBusinessListByName(page,businessQueryVo);
        //返回数据
        return Result.ok(page);
    }
    /**
     * 添加商家
     */
    @PostMapping("/add")
    public Result add(@RequestBody Business business){
        if (businessService.save(business)){
            return Result.ok().message("商家添加成功");
        }
        return Result.error().message("商家添加失败");
    }
    /**
     * 修改商家
     */
    @PutMapping("/update")
    public Result update(@RequestBody Business business){
            if (businessService.updateById(business)) {
                return Result.ok().message("商家修改成功");
            }else {
                return Result.error().message("商家修改失败");
            }
    }
    /**
     * 删除实时数据
     */
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable Integer id){
        if (businessService.removeById(id)){
            return Result.ok().message("商家删除成功");
        }else {
            return Result.error().message("商家删除失败");
        }
    }
    /**
     * 批量删除商家数据
     */
    @DeleteMapping("/deletes")
    public Result deletes(@RequestBody List<String> ids){
        if (businessService.removeByIds(ids)){
            return Result.ok().message("商家批量删除成功");
        }else {
            return Result.error().message("商家批量删除失败");
        }
    }


}

