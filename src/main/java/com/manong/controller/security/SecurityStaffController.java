package com.manong.controller.security;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.manong.entity.security.SecurityStaff;
import com.manong.service.security.SecurityStaffService;
import com.manong.utils.Result;
import com.manong.vo.query.security.SecurityStaffQueryVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/securityStaff")
public class SecurityStaffController {
    @Resource
    private SecurityStaffService securityStaffService;
    /**
     * 分页查询保安列表
     */
    @GetMapping("/list")
    public Result list(SecurityStaffQueryVo securityStaffQueryVo){
        //创建分页对象
        IPage<SecurityStaff> page = new Page<SecurityStaff>(securityStaffQueryVo.getPageNo(),securityStaffQueryVo.getPageSize());
        //调用分页查询方法
        securityStaffService.findSecurityStaffListByName(page,securityStaffQueryVo);
        //返回数据
        return Result.ok(page);
    }
    /**
     * 添加保安
     */
    @PostMapping("/add")
    public Result add(@RequestBody SecurityStaff securityStaff){
        if (securityStaffService.save(securityStaff)){
            return Result.ok().message("保安添加成功");
        }
        return Result.error().message("保安添加失败");
    }
    /**
     * 修改保安
     */
    @PutMapping("/update")
    public Result update(@RequestBody SecurityStaff securityStaff){
        if (securityStaffService.updateById(securityStaff)) {
            return Result.ok().message("保安修改成功");
        }else {
            return Result.error().message("保安修改失败");
        }
    }
    /**
     * 删除实时数据
     */
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable Integer id){
        if (securityStaffService.removeById(id)){
            return Result.ok().message("保安删除成功");
        }else {
            return Result.error().message("保安删除失败");
        }
    }
}

