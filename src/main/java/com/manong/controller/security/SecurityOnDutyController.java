package com.manong.controller.security;

import com.manong.entity.security.SecurityOnDuty;
import com.manong.service.security.SecurityOnDutyService;
import com.manong.utils.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/securityOnDuty")
public class SecurityOnDutyController {
    @Resource
    private SecurityOnDutyService securityOnDutyService;
    /**
     * 通过值班dutyId查询值班表
     */
    @GetMapping("/getSecurityOnDutyById/{dutyId}")
    public Result list(@PathVariable Integer dutyId){
        SecurityOnDuty obj=securityOnDutyService.findSecurityOnDutyById(dutyId);
        //返回数据
        return Result.ok(obj);
    }
}

