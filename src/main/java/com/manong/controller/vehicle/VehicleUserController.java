package com.manong.controller.vehicle;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.manong.entity.vehicle.VehicleUser;
import com.manong.service.vehicle.VehicleUserService;
import com.manong.utils.Result;
import com.manong.vo.query.vehicle.VehicleUserQueryVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/vehicleUser")
public class VehicleUserController {
    @Resource
    private VehicleUserService VehicleuserService;

    @GetMapping("/sexList")
    public Result sexList(VehicleUserQueryVo vehicleUserQueryVo){
        //创建分页对象
        IPage<VehicleUser> page = new Page<VehicleUser>(vehicleUserQueryVo.getPageNo(),vehicleUserQueryVo.getPageSize());
        //调用分页查询方法
        VehicleuserService.findVehicleUserBySex(page,vehicleUserQueryVo);
        //返回数据
        return Result.ok(page);
    }
    @GetMapping("/list")
    public Result list(VehicleUserQueryVo vehicleUserQueryVo){
        //创建分页对象
        IPage<VehicleUser> page = new Page<VehicleUser>(vehicleUserQueryVo.getPageNo(),vehicleUserQueryVo.getPageSize());
        //调用分页查询方法
        VehicleuserService.findVehicleUserByName(page,vehicleUserQueryVo);
        //返回数据
        return Result.ok(page);
    }
    @PostMapping("/add")
    public Result add(@RequestBody VehicleUser vehicleUser){
        if (VehicleuserService.save(vehicleUser)){
            return Result.ok().message("车辆用户添加成功");
        }
        return Result.error().message("车辆用户添加失败");
    }
    @PutMapping("/update")
    public Result update(@RequestBody VehicleUser vehicleUser){
        if (VehicleuserService.updateById(vehicleUser)) {
            return Result.ok().message("车辆用户修改成功");
        }else {
            return Result.error().message("车辆用户修改失败");
        }
    }
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable Integer id){
        if (VehicleuserService.removeById(id)){
            return Result.ok().message("车辆用户删除成功");
        }else {
            return Result.error().message("车辆用户删除失败");
        }
    }
}

