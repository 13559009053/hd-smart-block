package com.manong.controller.vehicle;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.manong.entity.vehicle.VehicleLocationInformation;
import com.manong.service.vehicle.VehicleLocationInformationService;
import com.manong.utils.Result;
import com.manong.vo.query.vehicle.VehicleLocationInformationQueryVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/vehicleLocationInformation")
public class VehicleLocationInformationController {
    @Resource
    private VehicleLocationInformationService vehicleLocationInformationService;

    @GetMapping("/list")
    public Result list(VehicleLocationInformationQueryVo vehicleLocationInformationQueryVo){
        //创建分页对象
        IPage<VehicleLocationInformation> page = new Page<VehicleLocationInformation>(vehicleLocationInformationQueryVo.getPageNo(),vehicleLocationInformationQueryVo.getPageSize());
        //调用分页查询方法
        vehicleLocationInformationService.findVehicleLocationInformationByCarNumber(page,vehicleLocationInformationQueryVo);
        //返回数据
        return Result.ok(page);
    }
    @PostMapping("/add")
    public Result add(@RequestBody VehicleLocationInformation vehicleLocationInformation){
        if (vehicleLocationInformationService.save(vehicleLocationInformation)){
            return Result.ok().message("车辆位置添加成功");
        }
        return Result.error().message("车辆位置添加失败");
    }
    @PutMapping("/update")
    public Result update(@RequestBody VehicleLocationInformation vehicleLocationInformation){
        if (vehicleLocationInformationService.updateById(vehicleLocationInformation)) {
            return Result.ok().message("车辆位置修改成功");
        }else {
            return Result.error().message("车辆位置修改失败");
        }
    }
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable Integer id){
        if (vehicleLocationInformationService.removeById(id)){
            return Result.ok().message("车辆位置删除成功");
        }else {
            return Result.error().message("车辆位置删除失败");
        }
    }
}

