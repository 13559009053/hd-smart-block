package com.manong.controller.vehicle;

import com.manong.entity.vehicle.VehicleBasicInformation;
import com.manong.service.vehicle.VehicleBasicInformationService;
import com.manong.utils.Result;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

@RestController
@RequestMapping("/api/vehicleBasicInformation")
public class VehicleBasicInformationController {
    @Resource
    private VehicleBasicInformationService vehicleBasicInformationService;
    /**
     * 通过值班carNumber查询车辆基本信息
     */
    @GetMapping("/getVehicleBasicInformationByCarNumber/{carNumber}")
    public Result list(@PathVariable String carNumber){
        VehicleBasicInformation obj=vehicleBasicInformationService.findVehicleBasicInformationByCarNumber(carNumber);
        //返回数据
        return Result.ok(obj);
    }
}

