package com.manong.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.manong.entity.DeviceInformation;
import com.manong.service.DeviceInformationService;
import com.manong.service.SensorInformationService;
import com.manong.utils.Result;
import com.manong.vo.query.DeviceInformationQueryVo;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.ArrayList;

@RestController
@RequestMapping("/api/deviceInformation")
public class DeviceInformationController {
    @Resource
    private DeviceInformationService deviceInformationService;
    @Resource
    private SensorInformationService sensorInformationService;
    /**
     * 查询所有设备基本信息
     */
    @GetMapping("/listAll")
    public Result listAll(){
        return Result.ok(deviceInformationService.list());
    }
    /**
     * 分页查询设备列表
     */
    @GetMapping("/list")
    public Result list(DeviceInformationQueryVo deviceInformationQueryVo){
        //创建分页对象
        IPage<DeviceInformation> page = new Page<DeviceInformation>(deviceInformationQueryVo.getPageNo(),deviceInformationQueryVo.getPageSize());
        //调用分页查询方法
        deviceInformationService.findDeviceListByDeviceName(page,deviceInformationQueryVo);
        //返回数据
        return Result.ok(page);
    }
    /**
     * 通过设备编号单个查询设备信息
     */
    @GetMapping("/getDeviceInformationByDeviceId/{deviceId}")
    public Result getDeviceInformationByDeviceId(@PathVariable String deviceId){
        DeviceInformation deviceInformation = deviceInformationService.findDeviceInformationByDeviceId(deviceId);
        return Result.ok(deviceInformation);
    }
    /**
     * 通过设备name单个查询设备信息
     */
    @GetMapping("/getDeviceInformationByDeviceName/{deviceName}")
    public Result getDeviceInformationByDeviceName(@PathVariable String deviceName){
        ArrayList<DeviceInformation> list = deviceInformationService.findDeviceInformationByDeviceName(deviceName);
        return Result.ok(list);
    }
    /**
     * 设备添加
     */
    @PostMapping("/add")
    public  Result add(@RequestBody DeviceInformation deviceInformation){
        boolean n=deviceInformationService.add(deviceInformation);
        if(n){
            return Result.ok().message("设备添加成功");
        }
        return Result.error().message("设备添加失败");
    }
    /**
     * 修改设备
     */
    @PutMapping("/update")
    public Result update(@RequestBody DeviceInformation deviceInformation){
        boolean n=deviceInformationService.updateDeviceInformationById(deviceInformation);
        if(n){
            return Result.ok().message("设备修改成功");
        }
        return Result.error().message("设备修改失败");
    }
    /**
     * 删除角色
     */
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable String id){
        if (sensorInformationService.findSensorInformationByDeviceId(id)!=null){
            return Result.error().message("设备删除失败设备被绑定先删除子表");
        }else {
            if(deviceInformationService.deleteDeviceInformationById(id)){
                return Result.ok().message("设备删除成功");
            }
            return Result.error().message("设备删除失败");
        }
    }
}

