package com.manong.controller.keyPersonnel;


import com.manong.entity.keyPersonnel.KeyPersonnelLocationInformation;
import com.manong.service.keyPersonnel.KeyPersonnelLocationInformationService;
import com.manong.utils.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/keyPersonnelLocationInformation")
public class KeyPersonnelLocationInformationController {
    @Resource
    private KeyPersonnelLocationInformationService keyPersonnelLocationInformationService;
    /**
     * 通过值班personnelId查询值班表
     */
    @GetMapping("/getKeyPersonnelLocationInformationByPersonnelId/{personnelId}")
    public Result list(@PathVariable Integer personnelId){
        KeyPersonnelLocationInformation obj=keyPersonnelLocationInformationService.findKeyPersonnelLocationInformationByPersonnelId(personnelId);
        //返回数据
        return Result.ok(obj);
    }
    /**
     * 查询所有重点人员位置信息
     */
    @GetMapping("/listAll")
    public Result listAll(){
        return Result.ok(keyPersonnelLocationInformationService.list());
    }
}

