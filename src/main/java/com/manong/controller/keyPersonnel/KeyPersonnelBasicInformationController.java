package com.manong.controller.keyPersonnel;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.manong.entity.business.Business;
import com.manong.entity.keyPersonnel.KeyPersonnelBasicInformation;
import com.manong.service.keyPersonnel.KeyPersonnelBasicInformationService;
import com.manong.utils.Result;
import com.manong.vo.query.BusinessQueryVo;
import com.manong.vo.query.keypersonnel.KeyPersonnelQueryVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/keyPersonnelBasicInformation")
public class KeyPersonnelBasicInformationController {
    @Resource
    private KeyPersonnelBasicInformationService keyPersonnelBasicInformationService;

    /**
     * 通过危险程度分页查询重点人员列表
     */
    @GetMapping("/hazardDegreeList")
    public Result hazardDegreeList(KeyPersonnelQueryVo keyPersonnelQueryVo){
        //创建分页对象
        IPage<KeyPersonnelBasicInformation> page = new Page<KeyPersonnelBasicInformation>(keyPersonnelQueryVo.getPageNo(),keyPersonnelQueryVo.getPageSize());
        //调用分页查询方法
        keyPersonnelBasicInformationService.findKeyPersonnelByHazardDegree(page,keyPersonnelQueryVo);
        //返回数据
        return Result.ok(page);
    }
    //查询所有重点人员基本信息
    @GetMapping("/listAll")
    public Result listAll(){
        //返回数据
        return Result.ok(keyPersonnelBasicInformationService.list());
    }
    @GetMapping("/list")
    public Result list(KeyPersonnelQueryVo keyPersonnelQueryVo){
        //创建分页对象
        IPage<KeyPersonnelBasicInformation> page = new Page<KeyPersonnelBasicInformation>(keyPersonnelQueryVo.getPageNo(),keyPersonnelQueryVo.getPageSize());
        //调用分页查询方法
        keyPersonnelBasicInformationService.findKeyPersonnelListByName(page,keyPersonnelQueryVo);
        //返回数据
        return Result.ok(page);
    }
    @PostMapping("/add")
    public Result add(@RequestBody KeyPersonnelBasicInformation keyPersonnelBasicInformation){
        if (keyPersonnelBasicInformationService.save(keyPersonnelBasicInformation)){
            return Result.ok().message("重点人员添加成功");
        }
        return Result.error().message("重点人员添加失败");
    }
    @PutMapping("/update")
    public Result update(@RequestBody KeyPersonnelBasicInformation keyPersonnelBasicInformation){
        if (keyPersonnelBasicInformationService.updateById(keyPersonnelBasicInformation)) {
            return Result.ok().message("重点人员修改成功");
        }else {
            return Result.error().message("重点人员修改失败");
        }
    }
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable Integer id){
        if (keyPersonnelBasicInformationService.removeById(id)){
            return Result.ok().message("重点人员删除成功");
        }else {
            return Result.error().message("重点人员删除失败");
        }
    }
}

